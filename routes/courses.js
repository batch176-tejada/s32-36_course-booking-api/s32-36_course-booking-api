const express = require('express');
const route = express.Router();
const courseController = require('../controllers/courses');
const auth = require('../auth');


const {verify, verifyAdmin} = auth;

//rout-post
	route.post('/register',verify, verifyAdmin,(req,res) => {
		const course = req.body;
		console.log(course);
		courseController.register(course).then(result =>{
			res.send(result);
		})
	});

//rout-retrieve all courses
		route.get('/allCourses',verify, verifyAdmin,(req,res) => {
		courseController.getAllCourses({}).then(result =>{
			res.send(result);
		})
	});


//rout-retrieve all courses (ACTIVE-ONLY)
	route.get('/activeCourses',(req,res) => {
		courseController.getActiveCourses({}).then(result =>{
			res.send(result);
		})
	});


//rout-retrieve specific course
route.get("/specCourses/:id",(req, res) =>{
	let xCourse = req.params.id;
	courseController.getspecCourse(xCourse).then(sCourseId => res.send(sCourseId));
});

//rout-update specific course
	route.put("/updateCourses/:id",verify, verifyAdmin,(req, res) =>{
		let updateCourse = req.params.id;
		let updateCBody = req.body;
		courseController.getspecCourse(updateCourse, updateCBody).then(updatedCourseId => res.send(updatedCourseId));
	});


//Archiving a course (soft delete can be recover)
	route.put('/archive/:id', verify, verifyAdmin, (req, res) => {
		let archiveCourse = req.params.id;
		courseController.archiveCourse(archiveCourse).then(result => res.send(result));
	})


module.exports = route;