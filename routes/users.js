//dependencies
	const express =	require('express');
	const controller = require('../controllers/users');
	const auth = require('../auth')

//routing component
	const route = express.Router();



//rout-post
	route.post('/register',(req,res) => {
		const stud = req.body;
		console.log(stud);
		controller.register(stud).then(result =>{
			res.send(result);
		})
	});

//route-user-auth
	route.post('/login',(req,res) =>{
		const accessOne = req.body;
		controller.userLogIn(accessOne).then(result => res.send(result));
	})


//route-get-user details
	route.get('/details', auth.verify, (req, res) =>{
		const iD = req.user.id;
		controller.getProfile({userId: iD}).then(result => res.send(result))
	})	

//Enroll the registered Users
	route.post('/enroll', auth.verify, controller.enroll);


//Get logged user's enrollments
route.get('/getEnrollments', auth.verify, controller.getEnrollments);




//Expose Route System
	module.exports = route;