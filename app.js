//[SECTION] Dependencies and Modules
	const express = require('express');
	const mongoose = require('mongoose');
	const dotenv = require('dotenv');
	const cors = require('cors');
  	const userRoutes = require('./routes/users');
  	const courseRoutes = require('./routes/courses');

//[SECTION] Environment Setup
	dotenv.config();
	let letsGo = process.env.SERVERD;
	const port = process.env.PORT;			 

//[SECTION] Server Setup
	const app = express();
	app.use(express.json());
	app.use(cors());

//[SECTION] Database Connection 
	mongoose.connect(letsGo, {
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	)
		const connectStatus = mongoose.connection;
		connectStatus.once('open', () => console.log(`Database Connected`));

//[SECTION] Backend Routes 
	app.use('/users',userRoutes);
	app.use('/courses',courseRoutes);
	

//[SECTION] Server Gateway Response

	app.get('/',(req,res)=>{
		res.send('WELCOME to The Orbital Admission')
	});
	app.listen(port, () => { 
		console.log(`API is hosted in port ${port}`); 
	});