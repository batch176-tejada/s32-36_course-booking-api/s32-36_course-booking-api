//[SECTION]
	const User = require('../models/User');
	const Course = require('../models/Course');
	const bcrypt = require('bcrypt');
	const dotenv = require('dotenv');
	const auth = require('../auth.js');

//[SECTION]Environmental Variables Setup
	dotenv.config();
	const naCl = parseInt(process.env.SALT);
	//const naCl = Number(process.env.SALT);
//[SECTION]create
	module.exports.register = (userData) => {
		let fName = userData.firstName;	
		let lName = userData.lastName;
		let email = userData.email;
		let passW = userData.password;
		let mobiNo = userData.mobileNo;
	
		let newUser = new User({
				firstName: fName,
				lastName: lName,
				email: email,
				password: bcrypt.hashSync(passW,naCl),
				mobileNo: mobiNo
		});
		return newUser.save().then((caught,denied) => {
			if (caught) {
				return caught;
			} 
			else {
				return {message:'Failed to Register account'};
			};
		});
	};

//[SECTION]User-Authentication
	module.exports.userLogIn = (data) =>{
		return User.findOne({email: data.email}).then(result => {
			if (result == null) {
				return false
			} 
			else {
				const isPassWordRight = bcrypt.compareSync(data.password,result.password)

					if (isPassWordRight) {
						return {accessToken: auth.createAccessToken(result.toObject())}
					}
					else {
						return false
					};
			};
		});
	};

//[SECTION] Retrieve user details
	module.exports.getProfile = (data) =>{
		return User.findById(data.userId).then(result =>{
			result.password = '';
			return result;
		})
	}


//[SECTION]Enroll the registered Users
	module.exports.enroll = async (req,res) => {
		console.log("Test enroll route");
		console.log(req.user.id);
		console.log(req.body.courseId);

		if (req.user.Admin) {
			res.send({message: "Action Forbidden"})
		} 
		
		let isUserUpdated = await User.findById(req.user.id).then(user =>{
			let newEnrollment = {
				courseId : req.body.courseId
			}

		user.enrollments.push(newEnrollment);
		return user.save().then(user => true).catch(err => err.message)
			if(isUserUpdated !== true){
				return res.send({message: isUserUpdated})
			}	
		});

			let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {
					//create an object which will be push to enrollees array/field
					let enrollee = {
							userId: req.user.id
					}

					course.enrollees.push(enrollee);

					//save the course document
					return course.save().then(course => true).catch(err => err.message)


					if(isCourseUpdated !== true) {
							return res.send({ message: isCourseUpdated })
					}

		})


		//send message to the client that we have successfully enrolled our user if both isUserUpdated and isCourseUpdated contain the boolean true.

		if(isUserUpdated && isCourseUpdated) {
				return res.send({ message: "Enrolled Successfully" })
		}

}


//Get user's enrollments

module.exports.getEnrollments = (req, res) => {
		User.findById(req.user.id)
		.then(result => res.send(result.enrollments))
		.catch(err => res.send(err))
}