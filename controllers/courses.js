//[SECTION]
	const Course = require('../models/Course');
	const auth = require('../auth.js');

//[SECTION]create
	module.exports.register = (courseData) => {
		let courseName = courseData.name;	
		let description = courseData.description;
		let price = courseData.price;
	
		let newCourse = new Course({
				name: courseName,
				description: description,
				price: price

		});

		return newCourse.save().then((caught,denied) => {
			if (caught) {
				return caught;

			} 
			else {
				return {message:'Failed to Register account'};
			};
		});
	};

//[SECTION]retrieve all
	module.exports.getAllCourses = () => {
		return Course.find().then(result => {
			return result;
		})
	}


//[SECTION]retrieve all ACTIVE
	module.exports.getActiveCourses = () => {
		return Course.find({isActive: true}).then(result => {
			return result;
		}).catch(error => error)

	}

//[SECTION]retrieve specific ONLY
	module.exports.getspecCourse = (specCourse) => {
		return Course.findById(specCourse).then((display, err) => {

			if (display) {
				console.log(display);
				return {message: `Data Retrieve: ${display.name} @ ${display.price}`};
			} 
			else {
				return {message:`Error in Retrieving DATA`};
			};
		});
	};


//[SECTIOn]Update specific 
	module.exports.revisedCourse = (courseId,revisedCourse) => {
		let updatedCourse = {
			name: revisedCourse.name,
			description: revisedCourse.description,
			price: revisedCourse.price
		}

		return Course.findByIdAndUpdate(courseId, updatedCourse).then((modify,err) => {
				      if(error){
				        return false;
				      } 
				      else {
				        return true;
				      }
				    }).catch(error => error)
				  }

//[SECTIOn]Archive specific (SOFT DELETE)

	module.exports.archiveCourse = (courseId) => {
		let updateActiveField = {
		isActive: false
	};

	return Course.findByIdAndUpdate(courseId, updateActiveField).then((course, error) => {

		if(error){
			return false;
		} else {
			return true;
		}
	}).catch(error => error)
}


	
